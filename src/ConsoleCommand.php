<?php

namespace judahnator\HoldthisCli;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

abstract class ConsoleCommand extends Command
{

    final protected function initialize(InputInterface $input, OutputInterface $output)
    {
        if (option()->get('api_key')) {
            return;
        }
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $key = $helper->ask($input, $output, new Question('Enter your HoldThis.net API key: '));
        option()->set('api_key', $key);
    }

}