<?php

namespace judahnator\HoldthisCli;

use GuzzleHttp\Client;
use judahnator\Option\Drivers\JsonFileDriver;
use judahnator\Option\Option;

function guzzle(): Client
{
    static $client = null;
    if (is_null($client)) {
        $client = new Client([
            'base_uri' => 'https://holdthis.net/api/',
            'headers' => [
                'Authorization' => option()->get('api_key'),
                'Accept' => 'application/json'
            ]
        ]);
    }
    return $client;
}

function option(): Option
{
    static $option = null;
    if (is_null($option)) {
        $option = new Option(new JsonFileDriver(__DIR__.'/../config.json'));
    }
    return $option;
}