<?php

namespace judahnator\HoldthisCli\Commands;


use GuzzleHttp\Exception\GuzzleException;
use judahnator\HoldthisCli\ConsoleCommand;
use function judahnator\HoldthisCli\guzzle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Put extends ConsoleCommand
{

    protected function configure()
    {
        $this
            ->setName('put')
            ->setDescription('Upload a file.')
            ->addArgument('filename', InputArgument::REQUIRED, 'The file to upload.')
            ->addOption('visibility', null, InputOption::VALUE_OPTIONAL, 'The visibility for this file', 'unlisted')
            ->addOption('notes', null, InputOption::VALUE_OPTIONAL, 'Notes for this file', '');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filepath = getcwd() . '/' . $input->getArgument('filename');
        if (!file_exists($filepath)) {
            $output->writeln("<error>Cannot find file</error>");
            return;
        }
        try {
            guzzle()->request(
                'POST',
                'https://holdthis.net/api/files',
                [
                    'multipart' => [
                        [
                            'name'     => 'file',
                            'contents' => fopen($filepath, 'r')
                        ],
                        [
                            'name'     => 'visibility',
                            'contents' => $input->getOption('visibility')
                        ],
                        [
                            'name'     => 'notes',
                            'contents' => $input->getOption('notes')
                        ]
                    ]
                ]
            );
        } catch (GuzzleException $e) {
            $output->writeln([
                "<error>Could not upload file.</error>",
                $e->getMessage()
            ]);
            return;
        }
        $output->writeln("<info>File uploaded successfully!</info>");
    }

}