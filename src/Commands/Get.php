<?php

namespace judahnator\HoldthisCli\Commands;


use GuzzleHttp\Exception\RequestException;
use judahnator\HoldthisCli\ConsoleCommand;
use function judahnator\HoldthisCli\guzzle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Get extends ConsoleCommand
{

    protected function configure()
    {
        $this
            ->setName('get')
            ->setDescription('Get a file.')
            ->addArgument('filename', InputArgument::IS_ARRAY, 'The file (or files) to retrieve.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($input->getArgument('filename') as $file) {
            try {
                $response = json_decode(guzzle()->get("files/{$file}")->getBody());
                $output->writeln("Downloading {$response->clientName}...");
                guzzle()->get("files/{$file}/{$response->clientName}", ['save_to' => getcwd()."/{$response->clientName}"]);
            }catch (RequestException $e) {
                $output->writeln("<error>Cannot download file {$file}.</error>");
                return;
            }
        }
        $output->writeln('<info>Files finished downloading.</info>');
    }

}