<?php

namespace judahnator\HoldthisCli\Commands;


use GuzzleHttp\Exception\RequestException;
use judahnator\HoldthisCli\ConsoleCommand;
use function judahnator\HoldthisCli\guzzle;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Ls extends ConsoleCommand
{

    protected function configure()
    {
        $this
            ->setName('ls')
            ->setDescription('Lists the files.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $files = json_decode(guzzle()->get('files')->getBody());
        }catch (RequestException $exception) {
            $output->writeln("<error>An error occurred, check your API key.</error>");
            return;
        }
        $table = new Table($output);
        $table->setHeaders(['ID', 'Downloads', 'Size', 'Name']);
        $table->setRows(
            array_map(
                function(\stdClass $file): array
                {
                    return [
                        $file->id,
                        $file->downloads,
                        $file->file_size->pretty,
                        $file->clientName
                    ];
                },
                $files
            )
        );
        $table->render();
    }

}