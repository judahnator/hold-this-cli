<?php

namespace judahnator\HoldthisCli\Commands;


use function judahnator\HoldthisCli\option;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetApiKey extends Command
{

    protected function configure()
    {
        $this
            ->setName('set:api_key')
            ->setDescription('Sets the HoldThis API key.')
            ->setHelp('You will need an API key to use this app. You can get a key here: https://holdthis.net/tokens')
            ->addArgument('key', InputArgument::REQUIRED, 'The HoldThis API key.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        option()->set('api_key', $input->getArgument('key'));
        $output->writeln('<info>API key has been updated.</info>');
    }

}